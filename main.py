import random
import threading
from philosophers import Philosophers


def create_locks():
    locks = list()
    for _ in range(5):
        locks.append(threading.Lock())
    return locks


def start_eating():
    locks = create_locks()

    philosophers = list()
    for num in range(5):
        t = Philosophers(
            dinner_residue=6,
            number=num,
            left_lock=locks[(num + 1) % 5],
            right_lock=locks[num]
        )
        philosophers.append(t)

    random.shuffle(philosophers)
    for philosopher in philosophers:
        philosopher.start()

    for philosopher in philosophers:
        philosopher.join()


if __name__ == '__main__':

    start_eating()
    print("\033[92mDone! \033[0m")
