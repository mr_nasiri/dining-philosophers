import random
import threading
import time


class Philosophers(threading.Thread):

    def __init__(self, dinner_residue, number, left_lock, right_lock, *args, **kwargs):
        self.dinner_residue = dinner_residue
        self.left_lock = left_lock
        self.right_lock = right_lock
        self.number = number
        super().__init__(*args, **kwargs)

    def eating(self):
        if self.dinner_residue > 0:
            eating_time = random.randint(2, 3)
            time.sleep(eating_time)
            self.dinner_residue -= eating_time
            if self.dinner_residue < 0:
                self.dinner_residue = 0

    def thinking(self):
        thinking_time = random.randint(2, 3)
        time.sleep(thinking_time)
        while self.right_lock.locked() and self.left_lock.locked():
            pass

    def run(self):
        while self.dinner_residue > 0:

            if not self.right_lock.locked() and not self.left_lock.locked():

                self.right_lock.acquire()
                self.left_lock.acquire()

                print(f"\nphilosopher-{self.number}\033[92m start eating! \033[0m \n{'_' * 50}")
                self.eating()

                self.left_lock.release()
                self.right_lock.release()

                print(f"\nphilosopher-{self.number}\033[91m end eating!"
                      f"\033[95m  (dinner residue:{self.dinner_residue}) \033[0m \n{'_' * 50}")
                self.thinking()

            else:
                self.thinking()
