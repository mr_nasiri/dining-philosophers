# Dining philosophers problem
https://en.wikipedia.org/wiki/Dining_philosophers_problem

## Requirements
- Python 3.6+

## Quick Start
- Create a directory and name it "Dining philosophers"
- Open a terminal in this directory
- Create a virtualenv:

    ```shell script
    virtualenv -p python3 Venv
    ```
- Activate virtualenv:

    ```shell script
    sourse Venv/bin/activate
    ```

- Clone this project:

    ```shell script
    git clone https://gitlab.com/mr_nasiri/dining-philosophers src
    ```
- Change directory to "src":

    ```shell script
    cd src
    ```
- Run main.py and enjoy...
